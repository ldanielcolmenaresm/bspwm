#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

xrandr --output LVDS-1 --primary --mode 1366x768 --pos 0x128 --rotate normal --output VGA-1 --mode 1280x1024 --pos 1366x0 --rotate normal --output HDMI-1 --off --output DP-1 --off

$HOME/.config/polybar/launch.sh &

keybLayout=$(setxkbmap -v | awk -F "+" '/symbols/ {print $2}')

if [ $keybLayout = "be" ]; then
  run sxhkd -c ~/.config/bspwm/sxhkd/sxhkdrc-azerty &
else
  run sxhkd -c ~/.config/bspwm/sxhkd/sxhkdrc &
fi

feh --bg-fill ~/background/Ju5PuBC-arch-linux-wallpaper.jpg &
xsetroot -cursor_name left_ptr &
run albert &
run parcellite &
run variety &
run nm-applet &
run pamac-tray &
run xfce4-power-manager &
numlockx on &
blueberry-tray &
picom --config $HOME/.config/bspwm/picom.conf &
run volumeicon &
run cbatticon &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
